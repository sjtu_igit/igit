//
//  Entry.cpp
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "Entry.h"
#include "Constants.h"

namespace iGit{

	void Entry::Initialize(){
		if (FileSys::exists(iGitRepository)){
			FileSys::remove_all(iGitRepository);
		}
		FileSys::create_directory(iGitRepository);
		FileStorage::Initialize();
		WorkSpace::Initialize();
		Library::Initialize();
	}

	Entry::Entry() : Storage(), UserSpace(), VersionLibrary(){
		if (!FileSys::exists(iGitRepository)){
			throw iGitError(0x800);
		}
	}

	void Entry::Add(const std::string& StorePath) {
		FileSys::path FilePath(StorePath);
		UserSpace.AddToStage(FilePath);
	}
	
	void Entry::Remove(const std::string &FileName) {
		FileSys::path FilePath(FileName);
		UserSpace.RemoveFromStage(FilePath);
	}

	void Entry::Commit() {
		VersionLibrary.CommitVersion(UserSpace.CreateNewVersion(Storage));
	}

	void Entry::CheckOut(const std::string& VersionName) {
		UserSpace.CheckOut(VersionLibrary.GetConstVersion(VersionName), Storage);
		VersionLibrary.CheckOut(VersionName);
	}

	void Entry::Edit() {
		Version _Version = UserSpace.CreateNewVersion(Storage);
		VersionLibrary.EditVersion(_Version);
	}

	void Entry::Forge() {
		Version _Version = UserSpace.CreateNewVersion(Storage);
		VersionLibrary.ForgeVersion(_Version);
	}

	void Entry::Status(std::ostream& os) {	//查询iGit当前状态
		UserSpace.Status(os);
	}
	
	void Entry::VersionStatus(std::ostream &os, const std::string &VersionName) {
		const Version &_Version = VersionLibrary.GetConstVersion(VersionName);
		_Version.Output(os);
	}

	void Entry::History(std::ostream& os) {	//查询iGit的版本库
		std::vector<std::string> VersionList = VersionLibrary.GetVersionList();
		for (auto NameIterator = VersionList.begin(); NameIterator != VersionList.end(); ++NameIterator) {
			os << *NameIterator;
			if (*NameIterator == VersionLibrary.GetHeadVersion()) {
				os << " <- Head";
			}
			if (*NameIterator == VersionLibrary.GetMasterVersion()) {
				os << " <- Master";
			}
			os << std::endl;
		}
		os << std::endl;
	}

	void Entry::Diff(std::ostream& os, const std::string& VersionName1, const std::string& VersionName2) {	//比较参数2和参数3代表的版本
		VersionLibrary.DiffVersions(VersionName1, VersionName2, os);
	}
	
	void Entry::CleanUp() {
		VersionLibrary.RubbishClear(Storage);
	}

	void Entry::Run(std::ostream &os, const std::vector<std::string> &cmds){
		std::vector<std::string>::const_iterator cmd_pointer = cmds.begin();
		if (*cmd_pointer == "add"){
			++cmd_pointer;
			if (cmd_pointer == cmds.end()) {
				throw iGitError(0x802);
			}
			Add(*cmd_pointer);
		} else if(*cmd_pointer == "commit") {
			Commit();
		} else if(*cmd_pointer == "edit") {
			Edit();
		} else if(*cmd_pointer == "forge") {
			Forge();
		} else if(*cmd_pointer == "status") {
			++cmd_pointer;
			if (cmd_pointer == cmds.end()) {
				Status(std::cout);
			} else {
				VersionStatus(std::cout, *cmd_pointer);
			}
		} else if(*cmd_pointer == "history") {
			History(std::cout);
		} else if(*cmd_pointer == "checkout") {
			++cmd_pointer;
			if (cmd_pointer == cmds.end()) {
				throw iGitError(0x802);
			}
			CheckOut(*cmd_pointer);
		} else if(*cmd_pointer == "rm") {
			++cmd_pointer;
			if (cmd_pointer == cmds.end()) {
				throw iGitError(0x802);
			}
			Remove(*cmd_pointer);
		} else if(*cmd_pointer == "diff") {
			++cmd_pointer;
			if (cmd_pointer == cmds.end()) {
				throw iGitError(0x802);
			}
			std::string str1 = *cmd_pointer;
			++cmd_pointer;
			if (cmd_pointer == cmds.end()) {
				throw iGitError(0x802);
			}
			std::string str2 = *cmd_pointer;
			Diff(std::cout, str1, str2);
		} else if(*cmd_pointer == "clean-up") {
			CleanUp();
		} else {
			throw iGitError(0x801);
		}
	}

	void Entry::Analysis(std::ostream &os, const std::vector<std::string> &cmds){
		srand((unsigned int)time(0));
		try{
			unsigned int total_cmds = cmds.size();
			if(total_cmds >= 1){
				if (cmds[0] == "init") {
					Initialize();
				} else {
					Entry().Run(os, cmds);
				}
			}
		} catch (iGitError e) {
			throw e;
		} catch (FileSys::filesystem_error e) {
			throw iGitError(0x803);
		}
	}

}
