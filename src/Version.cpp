//
//  Version.cpp
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "Version.h"
#include "Constants.h"

namespace iGit{

	Version::Version() : FileList(), VersionName(OriginVersionName), PreVersion(NULLVersionName), NextVersion(NULLVersionName) {
	}
	Version::Version(std::istream &is) {	//从流中读入一个Version的构造
		int N;
		is >> N;
		for (int i = 0; i < N; ++i) {
			FileList.push_back(FilePointer(is));
		}
		is >> VersionName >> PreVersion >> NextVersion;
	}
	Version::Version(const std::vector<FilePointer>& _FileList) : FileList(_FileList), PreVersion(NULLVersionName), NextVersion(NULLVersionName) {	//创建新版本，PreVersion和NextVersion均设置为空，即"*"，随机赋一个VersionName
		int n = RandomVersionNameLength;
		VersionName = "";
		for (int i = 0; i < n; ++i) {
			VersionName += char(rand() % 26 + 'A');
		}
	}
	
	void Version::CopyFileList(const Version& _Version) {
		FileList.clear();
		FileList = _Version.FileList;
	}
	
	void Version::Statistic(std::set<std::string> &Hashes) const {
		for (Container::const_iterator it = FileList.begin(); it != FileList.end(); ++it) {
			Hashes.insert(it->GetFileHash());
		}
	}
	
	const std::vector<FilePointer>& Version::GetFileList() const {	//获得文件列表
		return FileList;
	}
	const std::string& Version::GetVersionName() const {	//获得版本名称
		return VersionName;
	}
	const std::string& Version::GetPreVersion() const {	//获得前一个版本名称
		return PreVersion;
	}
	const std::string& Version::GetNextVersion() const {	//获得后一个版本名称
		return NextVersion;
	}

	void LinkVersions(Version& Version1, Version& Version2) {
		Version1.NextVersion = Version2.VersionName;
		Version2.PreVersion = Version1.VersionName;
	}
	
	void Version::Output(std::ostream &os) const {
		unsigned long N = FileList.size();
		os << N << std::endl;
		for (Container::const_iterator it = FileList.begin(); it != FileList.end(); ++it) {
			it->Output(os);
		}
		os << VersionName << ' ' << PreVersion << ' ' << NextVersion << std::endl;
	}
	
}
