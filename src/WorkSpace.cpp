//
//  WorkSpace.cpp
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "WorkSpace.h"
#include "Constants.h"
#include "iGitUtilities.h"
#include <iomanip>

namespace iGit{
	
	void WorkSpace::Initialize(){
		Stage::Initialize();
	}

	WorkSpace::WorkSpace() : StageObject(){
	}

	const std::map<FileSys::path, std::string>& WorkSpace::GetStageFile() const {
		return StageObject.GetTrackList();
	}
	
	void WorkSpace::PrivateAddToStage(const FileSys::path &FileName) {
		if (FileSys::is_directory(FileName)) {
			for (FileSys::directory_iterator it(FileName), END; it != END; ++it) {
				PrivateAddToStage(it->path());
			}
		} else {
			StageObject.AddFile(DropPre(iGitCurrentPath, FileName));
		}
	}

	void WorkSpace::AddToStage(const FileSys::path& FileName){
		if (IsPrefix(iGitRepository, DropPre(iGitCurrentPath, FileName))) {
			throw iGitError(0x701);
		}
		if (FileName == iGitCurrentPath) {
			//std::cerr << "Attention!" << std::endl;
			for (FileSys::directory_iterator it(FileName), END; it != END; ++it) {
				if (iGitCurrentPath / iGitRepository != it->path()) {
					//std::cerr << it->path().string() << std::endl;
					PrivateAddToStage(it->path());
				}
			}
		} else {
			PrivateAddToStage(FileName);
		}
	}
	
	void WorkSpace::RemoveFromStage(const FileSys::path &FileName) {
		StageObject.RemoveFile(DropPre(iGitCurrentPath, FileName));
	}
	
	void WorkSpace::CheckOut(const iGit::Version& NowVersion, const FileStorage& Storage){
		const std::vector<FilePointer>& FileList = NowVersion.GetFileList();
		StageObject.CheckOut(FileList);
		ClearWorkingDirectory();
		for (std::vector<FilePointer>::const_iterator it = FileList.begin(); it != FileList.end(); ++it) {
			if (!FileSys::exists(iGitCurrentPath / it->GetFileName().parent_path())) {
				FileSys::create_directories(iGitCurrentPath / it->GetFileName().parent_path());
			}
			Storage.Materialize(iGitCurrentPath, *it);
		}
	}

	Version WorkSpace::CreateNewVersion(const FileStorage& Storage){
		return Version(StageObject.CreateCommitList(Storage));
	}

	void WorkSpace::ClearWorkingDirectory(){
		for (FileSys::directory_iterator it(iGitCurrentPath), END; it != END; ++it) {
			if(it->path() != iGitCurrentPath / iGitRepository){
				FileSys::remove_all(it->path());
			}
		}
	}

	void WorkSpace::Status(std::ostream& os) const {	//查询Workspace状态
		os << "File in stage:" << std::endl;
		std::map<FileSys::path, std::string> s;
		for (FileSys::directory_iterator it(iGitCurrentPath), END; it != END; ++it) {
			if (!FileSys::is_directory(it->path())) {
				s[it->path()] = GetFileHash(it->path());
			} else if (it->path() != iGitCurrentPath / iGitRepository) {
				for	(FileSys::recursive_directory_iterator it0(it->path()), END; it0 != END; ++it0) {
					if (!FileSys::is_directory(it0->path())) {
						s[it0->path()] = GetFileHash(it0->path());
					}
				}
			}
		}
		std::map<FileSys::path, std::string> StageFile = GetStageFile();
		for (std::map<FileSys::path, std::string>::const_iterator it = StageFile.begin(); it != StageFile.end(); ++it) {
			FileSys::path RelativeName = iGitCurrentPath / it->first;
			os << std::setw(4) << ' ';
			os << it->first.string();
            std::map<FileSys::path, std::string>::const_iterator it0 = s.find(RelativeName);
			if (it0 == s.end() || it0->second != it->second) {
				os << "*";
			}
            s.erase(RelativeName);
			os << std::endl;
		}
		os << std::endl;
		if (!s.empty()) {
			os << "Untracked File:" << std::endl;
			for (std::map<FileSys::path, std::string>::iterator it = s.begin(); it != s.end(); ++it) {
				os << std::setw(4) << ' ' << DropPre(iGitCurrentPath, it->first).string() << std::endl;
			}
		}
	}

	WorkSpace::~WorkSpace(){
	}

}
