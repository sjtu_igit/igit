//
//  Stage.cpp
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "Stage.h"
#include "Constants.h"
#include "iGitUtilities.h"

namespace iGit{

	void Stage::Initialize(){

		FileSys::create_directory(StageDirectory);

		std::ofstream SettingFile(StageSettingFile.string().c_str());

		SettingFile << 0 << std::endl;

		SettingFile.close();
	}

	Stage::Stage() {
		std::ifstream SettingFile(StageSettingFile.string().c_str());

		int Total = 0;
		std::string FilePath, FileHash;
		SettingFile >> Total;

		for (int i = 0; i < Total; ++i){
			SettingFile >> FilePath >> FileHash;
			TrackList[FilePath] = FileHash;
		}

		SettingFile.close();
	}

	Stage::~Stage(){
		std::ofstream SettingFile(StageSettingFile.string().c_str());

		SettingFile << TrackList.size() << std::endl;

		for(Container::iterator it = TrackList.begin(); it != TrackList.end(); ++it){
			SettingFile << it->first.string() << ' ' << it->second << std::endl;
		}

		SettingFile.close();
	}

	const Stage::Container& Stage::GetTrackList() const {
		return TrackList;
	}

	void Stage::AddFile(const FileSys::path &FileName){
		if (!FileSys::exists(FileName)) {
			throw iGitError(0x610);
		}
		if (FileSys::is_directory(FileName)) {
			throw iGitError(0x611);
		}
		if (FileName.begin() == iGitRepository.begin()) {
			throw iGitError(0x612);
		}


		if (!FileSys::exists(StageDirectory / FileName.parent_path())) {	//如果前一个目录不存在的话我就必须要把它创建出来。
			FileSys::create_directories(StageDirectory / FileName.parent_path());
		}
		FileSys::copy_file(FileName, StageDirectory / FileName, FileSys::copy_option::overwrite_if_exists);
		TrackList[FileName] = GetFileHash(FileName);
	}
	
	void Stage::RemoveFile(const FileSys::path &FileName) {
		for (Container::iterator it(TrackList.begin()); it != TrackList.end();) {
			if (IsPrefix(FileName, it->first)) {
				it = TrackList.erase(it);
			} else {
				++it;
			}
		}
	}
	
	std::vector<FilePointer> Stage::CreateCommitList(const FileStorage &Storage){
		std::vector<FilePointer> CommitList;
		for (Container::iterator it = TrackList.begin(); it != TrackList.end(); ++it) {
			CommitList.push_back(FilePointer(it->first, it->second));
			if (FileSys::exists(StageDirectory / it->first)) {
				Storage.PushFile(StageDirectory, FilePointer(it->first, it->second));
			}
		}

		ClearDirectory(StageDirectory);

		return CommitList;
	}

	void Stage::CheckOut(const std::vector<FilePointer>& FileList){
		TrackList.clear();

		for (std::vector<FilePointer>::const_iterator it = FileList.begin(); it != FileList.end(); ++it) {
			TrackList[it->GetFileName()] = it->GetFileHash();
		}

		ClearDirectory(StageDirectory);
	}

}
