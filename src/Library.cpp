//
//  Library.cpp
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "Library.h"
#include "iGitUtilities.h"

namespace iGit {
	class Node {
	private:
		int id;
		std::map<char, Node*> son;
	public:
		Node() : id(0) {}
		int GetId() {
			return id;
		}
		const std::map<char, Node*> GetSonList() {
			return son;
		}
		Node *GetSon(char x) {
			if (son.find(x) == son.end()) return NULL;
			else return son[x];
		}
		Node *AddSon(char x, int id) {
            son[x] = new Node;
            son[x]->id = id;
            return son[x];
		}
	};

	class Tree {
	private:
		int size;
		Node *root;
		void del(Node *p) {
			const std::map<char, Node*> SonList = p->GetSonList();
			for (std::map<char, Node*>::const_iterator it = SonList.begin(); it != SonList.end(); ++it) {
				del(it->second);
			}
			delete p;
		}
	public:
		Tree() {
			size = 0;
			root = new Node;
		}
		int add(char* &p) {
			Node *now = root;
			while (*p != '\n' && *p != '\0') {
				Node *tmp = now->GetSon(*p);
				if (tmp == NULL) {
					now = now->AddSon(*p, ++size);
				}
				else now = tmp;
				++p;
			}
			if (*p != '\0') ++p;
			return now->GetId();
		}
		~Tree() {
			del(root);
		}
	};

	bool cmp(std::pair<FilePointer, int> a, std::pair<FilePointer, int> b) {
		if (a.first.GetFileName().string() != b.first.GetFileName().string()) {
			return a.first.GetFileName().string() < b.first.GetFileName().string();
		}
		else return a.second < b.second;
	}

	void Library::Initialize(){	//初始化，创建空版本。
		std::ofstream SettingFile(LibrarySettingFile.string().c_str());

		SettingFile << 1 << std::endl;

		Version().Output(SettingFile);

		SettingFile << OriginVersionName << ' ' << OriginVersionName << std::endl;

		SettingFile.close();
	}

	Library::Library() {//构造函数，参数为工作目录，从文件中提取版本信息。
		std::ifstream SettingFile(LibrarySettingFile.string().c_str());

		int Total = 0;
		SettingFile >> Total;

		for (int i = 0; i < Total; ++i){
			Version _Version(SettingFile);
			VersionMap[_Version.GetVersionName()] = _Version;
		}
		SettingFile >> HeadVersion >> MasterVersion;

		SettingFile.close();
	}

	Library::~Library(){	//析构函数，其中要将版本信息输出到文件中
		std::ofstream SettingFile(LibrarySettingFile.string().c_str());

		SettingFile << VersionMap.size() << std::endl;

		for(Container::iterator it = VersionMap.begin(); it != VersionMap.end(); ++it){
			it->second.Output(SettingFile);
		}
		SettingFile << HeadVersion << ' ' << MasterVersion << std::endl;

		SettingFile.close();
	}

	std::string Library::GetFullName(const std::string &VersionName) const {
		if (*(VersionName.begin()) == '-' || *(VersionName).begin() == '+') {
			std::string ret = GetHeadVersion();
			for (std::string::const_iterator it = VersionName.begin(); it != VersionName.end(); ++it) {
				if (*it == '-') {
					ret = VersionMap.find(ret)->second.GetPreVersion();
				} else if(*it == '+') {
					ret = VersionMap.find(ret)->second.GetNextVersion();
				}
				if (ret == "*") {
					throw iGitError(0x501);
				}
			}
			return ret;
		} else if (VersionName == "Head") {
			return GetHeadVersion();
		} else if(VersionName == "Master") {
			return GetMasterVersion();
		} else {
			Container::const_iterator it = VersionMap.lower_bound(VersionName);
			if(it == VersionMap.end()){
				throw iGitError(0x501);
			}
			return it->first;
		}
	}
	
	void Library::RubbishClear(const iGit::FileStorage &Storage) const {
		std::set<std::string> Hashes;
		for (Container::const_iterator it = VersionMap.begin(); it != VersionMap.end(); ++it) {
			it->second.Statistic(Hashes);
		}
		Storage.RubbishClear(Hashes);
	}

	const Version& Library::GetConstVersion(const std::string& VersionName) const {
		return VersionMap.find(GetFullName(VersionName))->second;
	}

	Version& Library::GetVersion(const std::string& VersionName){
		return VersionMap.find(GetFullName(VersionName))->second;
	}

	Library::Container::iterator Library::PushNewVersion(const iGit::Version& NewVersion){
		return VersionMap.insert(std::make_pair(NewVersion.GetVersionName(), NewVersion)).first;
	}

	std::string Library::GetHeadVersion() const {
		return HeadVersion;
	}

	std::string Library::GetMasterVersion() const {
		return MasterVersion;
	}

	void Library::CheckOut(const std::string &VersionName) {
		HeadVersion = GetFullName(VersionName);
	}

	void Library::CommitVersion(const Version& _Version){	//在MasterVersion之后提交一个版本，并将MasterVersion指针指向新版本
		if (GetHeadVersion() != GetMasterVersion()) {
			throw iGitError(0x502);
		}

		Container::iterator it = PushNewVersion(_Version);
		LinkVersions(GetVersion(MasterVersion), it->second);

		MasterVersion = it->first;
		HeadVersion = it->first;
	}

	void Library::ForgeVersion(const Version& _Version){	//在某一个VersionName为string的Version之后插入const Version&
		if (GetHeadVersion() == GetMasterVersion()) {
			throw iGitError(0x503);
		}

		Version &pre = GetVersion(GetHeadVersion());
		Version &next = GetVersion(pre.GetNextVersion());
		Container::iterator it = PushNewVersion(_Version);

		LinkVersions(it->second, next);
		LinkVersions(pre, it->second);

		HeadVersion = it->first;
	}

	void Library::EditVersion(const Version& _Version){	//覆盖VersionName为string的Version
		if (GetHeadVersion() == OriginVersionName) {
			throw iGitError(0x504);
		}
		GetVersion(GetHeadVersion()).CopyFileList(_Version);
	}

	void Library::SetBack(const std::string&){	//将MasterVersion回退到某个历史版本，并且清空其它版本信息（垃圾回收）
	}

	std::vector<std::string> Library::GetVersionList() const {	//取得版本列表，按照版本拓扑顺序输出
		std::vector<std::string> ret;
		for (std::string Name = OriginVersionName; Name != NULLVersionName; Name = VersionMap.find(Name)->second.GetNextVersion()) {
			ret.push_back(Name);
		}
		return ret;
	}

	void Library::DiffVersions(const std::string& VersionName1, const std::string& VersionName2, std::ostream& os) const {
		Version Version1, Version2;
		Version1 = GetConstVersion(VersionName1);
		Version2 = GetConstVersion(VersionName2);
		std::vector<std::pair<FilePointer, int> > FileList;
		for (std::vector<FilePointer>::const_iterator it = Version1.GetFileList().begin(); it != Version1.GetFileList().end(); ++it) {
		    std::pair<FilePointer, int> tmp = std::pair<FilePointer, int>(*it, 1);
            FileList.push_back(tmp);
		}
		for (std::vector<FilePointer>::const_iterator it = Version2.GetFileList().begin(); it != Version2.GetFileList().end(); ++it) {
			std::pair<FilePointer, int> tmp = std::pair<FilePointer, int>(*it, 2);
			FileList.push_back(tmp);
		}
		std::sort(FileList.begin(), FileList.end(), cmp);
		for (std::vector<std::pair<FilePointer, int> >::iterator it = FileList.begin(); it != FileList.end(); ++it) {
			if (it + 1 != FileList.end()) {
				if (it->first.GetFileName().string() == (it + 1)->first.GetFileName().string()) {
					if (it->first.GetFileHash() == (it + 1)->first.GetFileHash()) {
						os << "There's no difference in " << it->first.GetFileName().string() << "." << std::endl;
					}
					else {
						os << it->first.GetFileName().string() << " are different:" << std::endl;
						const FileSys::path FilePath1 = FileStorageDirectory / it->first.GetFileHash();
						const FileSys::path FilePath2 = FileStorageDirectory / (it + 1)->first.GetFileHash();
						unsigned long FileSize;
						char *FileContent1 = ReadFile(FileSize, FilePath1);
						char *FileContent2 = ReadFile(FileSize, FilePath2);
						char *pointer1, *pointer2;
						pointer1 = FileContent1;
						pointer2 = FileContent2;
						std::vector<int> hash1, hash2;
						Tree tree;
						while (*FileContent1 != '\0') {
							hash1.push_back(tree.add(FileContent1));
						}
						while (*FileContent2 != '\0') {
							hash2.push_back(tree.add(FileContent2));
						}
						FileContent1 = pointer1;
						FileContent2 = pointer2;
						std::vector<std::pair<int, int> > LCS = GetLCS(hash1, hash2);
						int position1, position2;
						position1 = position2 = 0;
						for (int i = 0; i < (int)LCS.size(); ++i) {
							while (position1 != LCS[i].first) {
								os << std::setw(5) << ++position1 << std::setw(5) << ' ' << std::setw(2) << ' ';
								while (*pointer1 != '\n' && *pointer1 != '\0') {
									os << *(pointer1++);
								}
								os << std::endl;
								if (*pointer1 != '\0') ++pointer1;
							}
							while (position2 != LCS[i].second) {
								os << std::setw(10) << ++position2 << std::setw(2) << ' ';
								while (*pointer2 != '\n' && *pointer2 != '\0') {
									os << *(pointer2++);
								}
								os << std::endl;
								if (*pointer2 != '\0') ++pointer2;
							}
							os << std::setw(5) << ++position1 << std::setw(5) << ++position2 << std::setw(2) << ' ';
							while (*pointer1 != '\n' && *pointer1 != '\0') {
								os << *(pointer1++);
								++pointer2;
							}
							os << std::endl;
							if (*pointer1 != '\0') ++pointer1;
							if (*pointer2 != '\0') ++pointer2;
						}
						while (position1 != (int)hash1.size()) {
							os << std::setw(5) << ++position1 << std::setw(5) << ' ' << std::setw(2) << ' ';
							while (*pointer1 != '\n' && *pointer1 != '\0') {
								os << *(pointer1++);
							}
							os << std::endl;
							if (*pointer1 != '\0') ++pointer1;
						}
						while (position2 != (int)hash2.size()) {
							os << std::setw(10) << ++position2 << std::setw(2) << ' ';
							while (*pointer2 != '\n' && *pointer2 != '\0') {
								os << *(pointer2++);
							}
							os << std::endl;
							if (*pointer2 != '\0') ++pointer2;
						}
						delete[] FileContent1;
						delete[] FileContent2;
					}
					++it;
					continue;
				}
			}
			if (it->second == 1) os << "Only " << VersionName1 << " has the file " << it->first.GetFileName().string() << std::endl;
			if (it->second == 2) os << "Only " << VersionName2 << " has the file " << it->first.GetFileName().string() << std::endl;
		}
	}
}
