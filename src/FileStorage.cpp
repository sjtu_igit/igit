//
//  FileStorage.cpp
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "FileStorage.h"
#include "Constants.h"

namespace iGit{
	
	void FileStorage::Initialize(){
		FileSys::create_directory(FileStorageDirectory);
	}
	
	void FileStorage::PushFile(const FileSys::path& FromDirectory, const FilePointer& FileInfo) const{
		if (!FileSys::exists(FileStorageDirectory / FileInfo.GetFileHash())) {
			FileSys::copy_file(FromDirectory / FileInfo.GetFileName(), FileStorageDirectory / FileInfo.GetFileHash());
		}
	}
	
	void FileStorage::Materialize(const FileSys::path& ToDirectory, const iGit::FilePointer& FileInfo) const{
		FileSys::copy_file(FileStorageDirectory / FileInfo.GetFileHash(), ToDirectory / FileInfo.GetFileName());
	}
	
	void FileStorage::RubbishClear(const std::set<std::string> &Hashes) const {
		for (FileSys::directory_iterator it(FileStorageDirectory), END; it != END; ++it) {
			if (Hashes.find(it->path().filename().string()) == Hashes.end()) {
				FileSys::remove(it->path());
			}
		}
	}
	
}
