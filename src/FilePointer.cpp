//
//  FilePointer.cpp
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "FilePointer.h"

namespace iGit{

	FilePointer::FilePointer(std::istream &is) : FileName(), FileHash(){
		is >> FileName >> FileHash;
	}

	FilePointer::FilePointer(const FileSys::path& _FileName, const std::string& _FileHash) : FileName(_FileName), FileHash(_FileHash)
	{
	}

	const FileSys::path& FilePointer::GetFileName() const{
		return FileName;
	}

	const std::string& FilePointer::GetFileHash() const{
		return FileHash;
	}

	void FilePointer::Output(std::ostream &os) const{
		os << FileName.string() << ' ' << FileHash << std::endl;
	}

}

