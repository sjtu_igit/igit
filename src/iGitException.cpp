//
//  iGitException.cpp
//  iGit
//
//  Created by 陈天垚 on 4/27/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "iGitException.h"
#include "ErrorCode.h"

namespace iGit{

	iGitError::iGitError(int _ErrorCode){
		CodePos = 0;
		for (const CodePair *Pointer = CodeList; Pointer->first != EndOfErrCode; ++Pointer){
			if(_ErrorCode == Pointer->first){
				return;
			}
			++CodePos;
		}
	}

	const char* iGitError::what(){
		return CodeList[CodePos].second;
	}

}
