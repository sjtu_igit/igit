//
//  main.cpp
//  iGit
//
//  Created by 陈天垚 on 4/27/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include <iostream>
#include "iGitException.h"
#include "iGitUtilities.h"
#include "FilePointer.h"
#include "Entry.h"
#include "Resources.h"
using namespace iGit;

int main(int argc, const char * argv[]) {

	try{
		std::vector<std::string> cmds;
		if (argc > 1) {
			for (int i = 1; i < argc; ++i){
				cmds.push_back(argv[i]);
			}
		} else {
			int n;
			std::string str;
			std::cin >> n;
			for (int i = 0; i < n; ++i) {
				std::cin >> str;
				cmds.push_back(str);
			}
		}
		Entry::Analysis(std::cout, cmds);
	} catch(iGitError e) {
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
