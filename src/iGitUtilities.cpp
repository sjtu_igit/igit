//
//  iGitUtilities.cpp
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#include "iGitUtilities.h"
#include <boost/uuid/sha1.hpp>

namespace iGit{

	char* ReadFile(unsigned long &FileSize, const FileSys::path& FileName){
		FileSize = FileSys::file_size(FileName);
		std::ifstream File(FileName.string().c_str(), std::ios::binary | std::ios::in);

		char *Result = new char[FileSize + 1];
		File.read(Result, FileSize);
		Result[FileSize] = 0;

		File.close();
		return Result;
	}

	std::string TransFormat(const unsigned int* digest){
		std::stringstream SStream;
		SStream << std::hex;
		for (int i = 0; i < 5; ++i) {
			int x = digest[i];
			for (int j = 0; j < 8; ++j) {
				SStream << (x & 0xf);
				x >>= 4;
			}
		}
		std::string ret;
		SStream >> ret;
		return ret;
	}

	std::string GetFileHash(const FileSys::path& FileName){
		if (!FileSys::exists(FileName)) {
			throw iGitError(0x100);
		}

		unsigned long FileSize;

		char *FileContent = ReadFile(FileSize, FileName);	//读取文件内容
		boost::uuids::detail::sha1 ShaObject;
		unsigned int digest[5];
		ShaObject.process_bytes(FileContent, FileSize);	//将文件内容输入Sha1类型
		ShaObject.get_digest(digest);	//取得二进制哈希值

		delete[] FileContent;

		return TransFormat(digest);	//转换成字符类型
	}

	void ClearDirectory(const FileSys::path& Directory){

		if (!FileSys::exists(Directory)) {
			throw iGitError(0x110);
		}

		if (!FileSys::is_directory(Directory)) {
			throw iGitError(0x111);
		}

		FileSys::remove_all(Directory);
		FileSys::create_directory(Directory);
	}

	int get_max(const std::vector<int>& vec1, const std::vector<int>& vec2) {
		int ret = 0;
		for (int i = 0; i < (int)vec1.size(); ++i) {
			ret = std::max(ret, vec1[i]);
		}
		for (int i = 0; i < (int)vec2.size(); ++i) {
			ret = std::max(ret, vec2[i]);
		}
		return ret;
	}
	
	std::vector<std::pair<int, int> > GetLCS(const std::vector<int>& vec1, const std::vector<int>& vec2) {
		if (vec1.empty() || vec2.empty()) {
			return std::vector<std::pair<int, int> >();
		}
		// initilize pos and pointer
		std::vector<std::list<int> > pos;
		std::vector<std::list<int>::iterator> pointer;
		pos.resize(get_max(vec1, vec2) + 1);
		pointer.resize(pos.size());
		for (int i = 0; i < (int)vec2.size(); ++i) {
			pos[vec2[i]].push_back(i);
		}
		// calculate LCS
		int n = vec1.size(), m = vec2.size();
		std::vector<std::vector<int> > L;
		L.resize(m);
		for (int i = 0; i < m; ++i) {
			L[i].resize(n, -1);
		}
		int len = 0;
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				pointer[vec1[j]] = pos[vec1[j]].begin();
			}
			int x = 0, y = i;
			while (true) {
				int t = -1;
				while (pointer[vec1[y]] != pos[vec1[y]].end()) {
					t = *pointer[vec1[y]];
					if (x == 0 || t > L[x - 1][y - 1]) break;
					else ++pointer[vec1[y]];
				}
				if (x != 0 && t <= L[x - 1][y - 1]) t = -1;
				if (y != 0 && L[x][y - 1] != -1) {
					if (t == -1) t = L[x][y - 1];
					else t = std::min(t, L[x][y - 1]);
				}
				if (t == -1) break;
				L[x][y] = t;
				++x, ++y;
				if (x == m || y == n) break;
			}
			if (y == n) {
				len = x;
				break;
			}
		}
		// get the solution
		std::vector<std::pair<int, int> > ret;
		int p = n - 1;
		for (int i = len - 1; i >= 0; --i) {
			while (p && L[i][p - 1] == L[i][p]) --p;
			ret.push_back(std::pair<int, int>(p, L[i][p]));
			--p;
		}
		reverse(ret.begin(), ret.end());
		return ret;
	}
	
	FileSys::path DropPre(const FileSys::path &PathPre, const FileSys::path &PathComp) {
		FileSys::path::iterator itPre(PathPre.begin()), itComp(PathComp.begin());
		while (itPre != PathPre.end() && itComp != PathComp.end() && itPre->string() == itComp->string()) {
			++itPre;
			++itComp;
		}
		FileSys::path ret;
		for (; itComp != PathComp.end(); ++itComp) {
			ret /= itComp->string();
		}
		return ret;
	}
	
	bool IsPrefix(const FileSys::path &PathPre, const FileSys::path &PathComp) {
		FileSys::path::iterator itPre(PathPre.begin()), itComp(PathComp.begin());
		for (; itPre != PathPre.end() && itComp != PathComp.end(); ++itPre, ++itComp) {
			if (itPre->string() != itComp->string()) {
				return false;
			}
		}
		return itPre == PathPre.end();
	}
}
