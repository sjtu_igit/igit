//
//  Resources.h
//  iGit
//
//  Created by 陈天垚 on 4/27/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef iGit_Resources_h
#define iGit_Resources_h

#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <set>
#include <map>
#include <queue>
#include <vector>
#include <string>
#include <cstring>
#include <exception>
#include <sstream>
#include <list>
#include <iomanip>
#include "iGitException.h"
//using namespace std;

#include <boost/filesystem.hpp>
namespace FileSys = boost::filesystem;

#endif
