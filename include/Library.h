//
//  Library.h
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__Library__
#define __iGit__Library__

#include "Resources.h"
#include "Constants.h"
#include "FileStorage.h"
#include "Version.h"

namespace iGit{

	class Library{
		/*
			Library用于维护版本库和版本指针
		 */
	private:
		typedef std::map<std::string, Version> Container;
		Container VersionMap;	//版本库
		std::string HeadVersion, MasterVersion;	//当前工作指针（HeadVersion）和当前最新版本（MasterVersion）
		
		std::string GetFullName(const std::string&) const;	//给定一个在终端中对版本的表示，返回该表示在Library中对应的版本号全名。
		Version& GetVersion(const std::string&);	//给定一个终端中的版本表示，返回该版本的Version引用
		Container::iterator PushNewVersion(const Version&);	//将给定的版本添加到Library中
	public:
		static void Initialize();	//初始化，创建空版本。
		Library();	//构造函数，从文件中提取版本信息。
		~Library();	//析构函数，其中要将版本信息输出到文件中
		const Version& GetConstVersion(const std::string&) const;	//通过一个终端中的版本表示，获得该版本的Version引用
		void RubbishClear(const FileStorage&) const;	//清理不需要的文件

		std::string GetHeadVersion() const ;	//取得Head指针对应的版本号
		std::string GetMasterVersion() const ;	//取得Master指针对应的版本号
		void CheckOut(const std::string&);	//将Head指针指向所给定的版本名称。
		void CommitVersion(const Version&);	//在MasterVersion之后提交一个版本，并将MasterVersion指针指向新版本
		void ForgeVersion(const Version&);	//在某一个VersionName为string的Version之后插入const Version&
		void EditVersion(const Version&);	//覆盖VersionName为string的Version
		void SetBack(const std::string&);	//将MasterVersion回退到某个历史版本，并且清空其它版本信息（垃圾回收）

		std::vector<std::string> GetVersionList() const ;	//取得版本列表，按照版本拓扑顺序输出
		void DiffVersions(const std::string&, const std::string&, std::ostream&) const ;	//将参数1的版本与参数2的版本进行比较，输出到参数3中。
	};

}

#endif /* defined(__iGit__Library__) */
