//
//  FilePointer.h
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__FilePointer__
#define __iGit__FilePointer__

#include "Resources.h"

namespace iGit{

	class FilePointer{
	private:
		FileSys::path FileName;	//文件名称，是【相对目录】
		std::string FileHash;	//文件Hash
	public:
		FilePointer(std::istream&);	//从文件中读入一个FilePointer的构造
		FilePointer(const FileSys::path&, const std::string&);	//利用文件路径机器SHA1值构造一个FilePointer
		const FileSys::path& GetFileName() const;	//取得文件名称
		const std::string& GetFileHash() const;	//取得文件SHA1值
		void Output(std::ostream&) const;	//打印该FilePointer
	};

}

#endif /* defined(__iGit__FilePointer__) */
