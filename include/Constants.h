//
//  Constants.h
//  iGit
//
//  Created by 陈天垚 on 4/27/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef iGit_Constants_h
#define iGit_Constants_h

#include "Resources.h"

const FileSys::path iGitCurrentPath(".");
const FileSys::path iGitRepository(".iGit");
const FileSys::path StageDirectory(iGitRepository / "Stage");
const FileSys::path StageSettingFile(iGitRepository / "StageSetting.set");
const FileSys::path LibrarySettingFile(iGitRepository / "LibrarySetting.set");
const FileSys::path FileStorageDirectory(iGitRepository / "Storage");

const std::string OriginVersionName("Origin");
const std::string NULLVersionName("*");
const int RandomVersionNameLength = 15;

#endif
