//
//  WorkSpace.h
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__WorkSpace__
#define __iGit__WorkSpace__

#include "Resources.h"
#include "Version.h"
#include "Stage.h"
#include "FileStorage.h"

namespace iGit{

	class WorkSpace{
		/*
			WorkSpace用于管理工作区和Stage
			和Library的交互【不是】WorkSpace的工作
		 */
	private:
		Stage StageObject;

		void ClearWorkingDirectory();	//清空工作目录，但忽略.iGit文件夹
		void PrivateAddToStage(const FileSys::path &);
		void PrivateRemoveFromStage(const FileSys::path &);
	public:
		static void Initialize();
		WorkSpace();	//初始化时创建StageObject对象
		~WorkSpace();	//析构函数

		const std::map<FileSys::path, std::string>& GetStageFile() const;	//返回stage里的文件
		void AddToStage(const FileSys::path&);	//将参数1的文件添加到StageObject中
		void RemoveFromStage(const FileSys::path&);	//将参数1的文件从StageObject里移除
		void CheckOut(const Version&, const FileStorage&);	//获得某个版本，并且将这个版本添加到StageObject中
		Version CreateNewVersion(const FileStorage&);	//创建新的版本，并且清空Stage。
		void Status(std::ostream& os) const;
	};

}

#endif /* defined(__iGit__WorkSpace__) */
