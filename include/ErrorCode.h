//
//  ErrorCode.h
//  iGit
//
//  Created by 陈天垚 on 4/27/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef iGit_ErrorCode_h
#define iGit_ErrorCode_h

/*
 ErrorCode	代码标示符定义：
 0x0**	用于初始化异常
 0x1**	用于iGitUtilities异常
 0x2**	用于FilePointer异常
 0x3**	用于FileStorage异常
 0x4**	用于Version异常
 0x5**	用于Library异常
 0x6**	用于Stage异常
 0x7**	用于WorkSpace异常
 0x8**	用于Entry异常
 */

#include <utility>
typedef std::pair<int, const char*> CodePair;
const int EndOfErrCode = 0xfff;

const CodePair CodeList[] = {
	CodePair(0x000, "Code 0x000, Initialization Error."),
	
	CodePair(0x100, "Code 0x100, In Calcating Sha1 of File: File Does not Exist."),
	CodePair(0x110, "Code 0x110, In Clearing Directory: Directory Does not Exist."),
	CodePair(0x111, "Code 0x111, In Clearing Directory: Given Path is not a Directory."),
	
	CodePair(0x200, ""),
	
	CodePair(0x300, ""),
	
	CodePair(0x400, ""),
	
	CodePair(0x500, ""),
	CodePair(0x501, "Code 0x501, No such Version Name."),
	CodePair(0x502, "Code 0x502, Head pointer is not equal to Master version."),
	CodePair(0x503, "Code 0x503, Head pointer is equal to Master version."),
	CodePair(0x504, "Code 0x504, Head pointer is now at Origin Version."),
	
	CodePair(0x600, ""),
	CodePair(0x610, "Code 0x610, In \"add\" operation, no such File."),
	CodePair(0x611, "Code 0x611, In \"add\" operation, this Path is a Directory."),
	CodePair(0x612, "Code 0x612, In \"add\" operation, added file should not be in iGit Directory."),
	
	CodePair(0x700, ""),
	CodePair(0x701, "Code 0x701, .iGit Directory should be operated."),
	
	CodePair(0x800, "Code 0x800, Need to \"init\" the Directory Before Any Work."),
	CodePair(0x801, "Code 0x801, Undefined Command."),
	CodePair(0x802, "Code 0x802, Addtional Command Parameter Required."),
	CodePair(0x803, "Code 0x803, Filesystem Error."),
	
	CodePair(0xfff, "Undefined ErrorCode")
};

#endif
