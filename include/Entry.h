//
//  Entry.h
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__Entry__
#define __iGit__Entry__

#include "Resources.h"
#include "WorkSpace.h"
#include "Library.h"
#include "FileStorage.h"

namespace iGit{
	
	class Entry{
	private:
		
		FileStorage Storage;
		WorkSpace UserSpace;
		Library VersionLibrary;
		
		/*
			所有操作的实现分为三块
			第一块是合法性检查
			第二块是提取调用所有其它类中的函数所需要的参数
			第三块是调用其他类的函数
		 */
		
		//修改
		void Add(const std::string&);	//添加文件到Stage中。
		void Remove(const std::string&);	//将文件从stage中移除。
		void Commit();	//提交版本
		void CheckOut(const std::string&);	//使工作区恢复到参数1代表的版本中
		void Edit();	//用当前Stage中的版本覆盖参数1代表的版本
		void Forge();	//用当前Stage中的版本插入参数1所代表的版本之后的版本
		
		//查询
		void Status(std::ostream&);	//查询iGit当前状态
		void VersionStatus(std::ostream&, const std::string&);	//查询某个版本的状态
		void History(std::ostream&);	//查询iGit的版本库
		void Diff(std::ostream&, const std::string&, const std::string&);	//比较参数2和参数3代表的版本
		void CleanUp();
		
		Entry();	//初始化所有成员。
		void Run(std::ostream&, const std::vector<std::string>&);	//参数2代表main的参数列表，不包含argv[0]
		
	public:
		static void Initialize();
		static void Analysis(std::ostream&, const std::vector<std::string>&);
	};
	
}

#endif /* defined(__iGit__Entry__) */
