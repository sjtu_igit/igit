//
//  Stage.h
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__Stage__
#define __iGit__Stage__

#include "Resources.h"
#include "FilePointer.h"
#include "FileStorage.h"

namespace iGit{

	class Stage{
		/*
			Stage用于维护跟踪列表和暂存文件
			并且根据跟踪列表创建新版本
		 */
	private:
		typedef std::map<FileSys::path, std::string> Container;
		Container TrackList;	//跟踪列表
	public:
		static void Initialize();	//初始化，参数是当前工作目录
		Stage();	//构造函数，构造时需要从文件中提取跟踪列表信息
		~Stage();	//析构函数，将跟踪列表信息输出到文件中

		const Container& GetTrackList() const;
		void AddFile(const FileSys::path&);	//添加文件，并且自动计算Hash值
		void RemoveFile(const FileSys::path&);	//删除文件
		std::vector<FilePointer> CreateCommitList(const FileStorage&);	//创建Commit文件列表，并且清空Stage
		void CheckOut(const std::vector<FilePointer>&);	//参数1为文件列表，利用参数1创造跟踪列表。
	};

}

#endif /* defined(__iGit__Stage__) */
