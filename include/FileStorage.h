//
//  FileStorage.h
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__FileStorage__
#define __iGit__FileStorage__

#include "Resources.h"
#include "FilePointer.h"

namespace iGit{
	
	class FileStorage{
		/*
			FileStorage用于管理文件仓库
		 */
	public:
		static void Initialize();	//初始化，初始化的时候需要创建初始的文件仓库
		void PushFile(const FileSys::path&, const FilePointer&) const;	//添加文件，参数1为某个绝对目录，参数2中的FileName为相对目录
		void Materialize(const FileSys::path&, const FilePointer&) const;	//拷出文件，参数1为某个绝对目录，参数2中的FileName为相对目录
		void RubbishClear(const std::set<std::string>&) const;	//清理Storage中的垃圾，参数表示需要留下的SHA1值。
	};
	
}

#endif /* defined(__iGit__FileStorage__) */
