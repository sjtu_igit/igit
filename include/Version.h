//
//  Version.h
//  iGit
//
//  Created by 陈天垚 on 4/29/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__Version__
#define __iGit__Version__

#include "Resources.h"
#include "FilePointer.h"

namespace iGit{
	
	class Version{
	private:
		typedef std::vector<FilePointer> Container;
		Container FileList;
		std::string VersionName;
		std::string PreVersion, NextVersion;
	public:
		Version();	//产生原始版本
		Version(std::istream&);	//从文件中读入一个Version的构造
		Version(const std::vector<FilePointer>&);	//创建新版本，PreVersion和NextVersion均设置为空，即"*"，随机赋一个VersionName
		
		void CopyFileList(const Version&);	//从一个Version中复制文件列表（用于edit命令）
		void Statistic(std::set<std::string>&) const;	//统计文件列表中存在的文件（用于clean-up命令）
		
		const std::vector<FilePointer>& GetFileList() const;	//获得文件列表
		const std::string& GetVersionName() const;	//获得版本名称
		const std::string& GetPreVersion() const;	//获得前一个版本名称
		const std::string& GetNextVersion() const;	//获得后一个版本名称
		
		friend void LinkVersions(Version&, Version&);	//连接两个版本
		void Output(std::ostream&) const;
	};
	
	void LinkVersions(Version&, Version&);
	
}

#endif /* defined(__iGit__Version__) */
