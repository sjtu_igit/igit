//
//  iGitUtilities.h
//  iGit
//
//  Created by 陈天垚 on 4/28/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__iGitUtilities__
#define __iGit__iGitUtilities__

#include "Resources.h"

namespace iGit{
	char* ReadFile(unsigned long &FileSize, const FileSys::path& FileName);

	std::string GetFileHash(const FileSys::path&);

	void ClearDirectory(const FileSys::path&);

	std::vector<std::pair<int, int> > GetLCS(const std::vector<int>&, const std::vector<int>&);
	
	FileSys::path DropPre(const FileSys::path &, const FileSys::path &);
	
	bool IsPrefix(const FileSys::path &, const FileSys::path &);
}

#endif /* defined(__iGit__iGitUtilities__) */
