//
//  iGitException.h
//  iGit
//
//  Created by 陈天垚 on 4/27/15.
//  Copyright (c) 2015 陈天垚, 万诚. All rights reserved.
//

#ifndef __iGit__iGitException__
#define __iGit__iGitException__

namespace iGit{

	class iGitError{
	private:
		int CodePos;
	public:
		iGitError(int);
		const char* what();
	};
	
}

#endif /* defined(__iGit__iGitException__) */
