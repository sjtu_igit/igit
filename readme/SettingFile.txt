1、FilePointer结构体的存储方式
一行两个字符串，分别代表path和Hash
2、Version类的存储方式
第一行，一个整数n
接下来n行，每行一个FilePointer，表示vector Files.
最后一行三个string，分别代表当前，前一个，后一个版本的name。如果是空则用*表示
3、LibrarySettingFile的表示。
第一行一个整数n，表示当前一共有多少个版本
接下来n个Version类。
之后是两个string表示Head和Master对应的版本名称。

注：最早的那个版本（也就是空版本）的版本名定为“Origin”

